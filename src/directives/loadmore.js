export default {
  inserted(el, binding) {
    var loading = false;
    el.addEventListener(
      "scroll",
      () => {
        var func = binding.value;
        if (!func) return;
        var scrollHeight = el.scrollHeight;
        var scrollTop = el.scrollTop;
        var offsetHeight = el.offsetHeight;
        if (scrollTop >= scrollHeight - offsetHeight - 40) {
          if (!loading) {
            loading = true;
            var p = () => {
              loading = false;
            };
            func(p);
          }
        }
      },
      { passive: true }
    );
  }
};
