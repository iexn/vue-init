import axios from "@/config/axios";

// 获取接口数据
export const getUserInfo = () => {
  return axios.get("get_userinfo.url");
};
