import axios from "axios";

// 设置超时时间
axios.defaults.timeout = 5000;

// 设置Content-Type类型
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded;charset=UTF-8";

// 接口地址头
axios.defaults.baseURL = "/";

// 监听发起
axios.interceptors.request.use(
  config => {
    if (config.method === "get") {
      // config.data = qs.stringify(config.data);
      config.data = JSON.stringify(config.data);
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

//返回状态判断
axios.interceptors.response.use(
  res => {
    if (!res.data.success) {
      //这里根据接口返回的实际情况来写
      //这里是接口异常，可以统一弹窗提示错误信息
      //ElementUI.Message.error(res.data.message);
      return Promise.reject(res);
    }
    return res;
  },
  error => {
    //404等问题可以在这里处理
    return Promise.reject(error);
  }
);

// 创建post方法
let post = function(api, params) {
  return new Promise((resolve, reject) => {
    params = JSON.stringify(params);
    axios
      .post(api, params)
      .then(
        response => {
          resolve(response.data);
        },
        err => {
          reject(err.data);
        }
      )
      .catch(error => {
        reject(error);
      });
  });
};

// 创建get方法
let get = function(api, data) {
  return new Promise((resolve, reject) => {
    axios
      .get(api, { params: data })
      .then(
        response => {
          resolve(response.data);
        },
        err => {
          reject(err);
        }
      )
      .catch(error => {
        reject(error);
      });
  });
};

export default {
  post,
  get
};
